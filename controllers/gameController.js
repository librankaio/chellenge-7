const { user_game, user_game_room, user_game_history } = require('../models')
const bcrypt = require('bcrypt');
const { body, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');


module.exports = {
    list: async (req, res) => {
        try {
            const data = await user_game_room.findAll();
            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    createRoom: async (req, res) => {
        try {
            const user = await user_game.findOne({
                where: {
                    id: res.user.id
                }
            })
            const data = await user_game_room.create({
                number_room: req.body.number_room,
                user_game_id1: user.id
            })

            return res.json({
                data: data
            })

        } catch (error) {
            console.log(error)
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    fightRoom: async (req, res) => {
        try {
            const user = await user_game.findOne({
                where: {
                    id: res.user.id
                }
            })
            const room = await user_game_room.findOne({
                where: {
                    number_room: req.body.number_room
                }
            })
            const data = await user_game_room.update({
                number_room: req.body.number_room,
                user_game_id2: user.id
            }, {
                where: {
                    number_room: room.number_room
                }
            },)

            return res.json({
                data: data
            })

        } catch (error) {
            console.log(error)
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
}