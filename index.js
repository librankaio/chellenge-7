const express = require("express");
const app = express();
require("dotenv").config();
const cors = require("cors");
const port = process.env.PORT;
const router = require("./router");
//Setup file static
app.use(express.static("public"));
// Setup view engine as ejs
app.set('view engine', 'ejs');

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(router)

app.get('/', (req, res) => {
    return res.render('index')
})

app.listen(port, () => {
    console.log(`This app listening on port ${port}`);
});
